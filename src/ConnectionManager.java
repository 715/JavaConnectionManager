import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class ConnectionManager {

    static private ObjectOutputStream oos;
    static private ObjectInputStream ois;

    public static void main(String argv[]) throws java.io.IOException {
        // set up a pipe
        System.out.println("Pipe setup");
        PipedOutputStream pos1 = new PipedOutputStream();
        PipedInputStream pis1 = new PipedInputStream(pos1);

        PipedOutputStream pos2 = new PipedOutputStream();
        PipedInputStream pis2 = new PipedInputStream(pos2);

        System.out.println("Object creation");
        Receiver r = new Receiver(pis2, pos1, ois, oos);
        Sender s = new Sender(pis1, pos2, ois, oos);

        System.out.println("Thread execution");
        r.start();
        s.start();
    }

} // end CLASS ConnectionManager
