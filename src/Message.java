// E. Troudt -- Fall 2003
import java.io.Serializable;

class Message implements Serializable {

    // parts of the message
    String theMessage;  // a string
    String[] someLines;   // an array
    int someNumber;  // a primitive

    // So clients can say System.out.println( msg )
    public String toString() {
        String s = "Message: " + theMessage +
                "\nwith an array: ";
        for (String someLine : someLines) {
            s += someLine + " ";
        } // end FOR(i)

        s += "\nand a magic #: " + someNumber;

        return s;
    } // end METHOD toString

} // end CLASS Message
