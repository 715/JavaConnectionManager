Connection Manager 

Connection manager has a static object output stream `oos` and a static object input stream `ois`. 
Connection manager creates two piped output streams, `pos1` and `pos2`. 
It creates a piped input stream for each of those piped output streams. 
`pis1` corresponds to `pos1` and `pis2` corresponds to `pos2` respectively. 

Connection manager then instantiates a receiver with the parameters `pis2`, `pos1`, `ois`, `oos`. 
Connection manager also instantiates a sender with the parameters `pis1`, `pos2`, `ois`, `oos`. 

This way, the piped output stream in the sender is associated with the piped input stream of the receiver. 
Similarly, the piped output stream in the receiver is associated with the piped input stream of the sender. 

The receiver creates a message with 
a string `"Hey there!"`, 
a string array `{"uno", "dos", "tres"}` 
and an integer `64`. 
It sends the message with `oos.writeObject( m )`. 
The sender received the message with `Message m = (Message) ois.readObject();`
The actual values here are insignificant. 
When the sender receives the message, it simply prints it out to the console. 

The sender prepares a BufferedReader with the text from its own source file `Sender.java`. 
We could use any other text file to transmit as well. 
I've tried it with `README.md` plain text file as well as `Sender.class`. 
Connection manager can work with both those files. 
Basically, all we need is a string we can send and receive. 

You can see the status of builds of this project at 
[https://gitlab.com/715/JavaConnectionManager/builds](https://gitlab.com/715/JavaConnectionManager/builds)